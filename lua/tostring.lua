-- tostring

local keywords =
{
	["and"] = true;
    ["break"] = true;
    ["do"] = true;
    ["else"] = true;
    ["elseif"] = true;
    ["end"] = true;

    ["false"] = true;
    ["for"] = true;
    ["function"] = true;
  	["goto"] = true; 
	["if"] = true;
    ["in"] = true;

    ["local"] = true;
    ["nil"] = true;
    ["not"] = true;
    ["or"] = true;
    ["repeat"] = true;
    ["return"] = true;

    ["then"] = true;
	["true"] = true;
	["until"] = true;
	["while"] = true;
}

-- Checks if the string can be used directly as a field name.
local function is_valid_field_string(s)
	return 
		type(s) == "string"
		and not keywords[s]
		and s:find "^[%a_][%w_]*$"
end

-- The bulk of the work of dumping a table. Calls `repr`, making `repr` possibly recursive.
local function dump_table(e, cache)
	local i = 1
	local ret = {}
    local repr = repr -- NOTE: global access: repr must exist. It is created at the end of the file.

	for k, v in pairs(e) do
		if i == k then
			i = i + 1
			table.insert(ret, repr(v, cache))
		else
			i = nil
			if is_valid_field_string(k) then
				table.insert(ret, k .. "=" .. repr(v, cache))
			else
				table.insert(ret, string.format("[%s]=%s", repr(k, cache), repr(v, cache)))
			end
		end
	end

    return "{" .. table.concat(ret, ", ") .. "}"
end

-- Original `tostring` from Lua.
local tostring_lua = tostring

-- `tostring` for strings for literal input. The result is quoted and with proper escapes.
local function tostring_literal(l)
    return string.format("%q", l)
end

-- `tostring` for "objects"
local function tostring_object(o)
	return "<" .. tostring_lua(o) .. ">"
end

-- `tostring` for tables. If neither __tostring nor __name is present in its metatable, return the
-- table contents
local function tostring_table(e, cache)
	if cache[e] then return "{...}" end
	cache[e] = true
    local mt = getmetatable(e)
    if mt then 
		if rawget(mt, "__tostring") then
			return tostring_lua(e)
		elseif rawget(mt, "__name") then 
			return tostring_object(e)
		end
		-- else, fallthrough
    end
    return dump_table(e, cache)
end

-- Generate stringifier functions
local function stringifier(strfun)
    local types =
    {
        ["nil"] = tostring_lua;
        boolean = tostring_lua;
        number = tostring_lua;
        string = strfun;
        thread = tostring_object;
        ["function"] = tostring_object;
        userdata = tostring_object;
        table = tostring_table;
    }

    return function(e, cache)
        return types[type(e)](e, cache or {})
    end
end

tostring = stringifier(tostring_lua)
repr = stringifier(tostring_literal)
